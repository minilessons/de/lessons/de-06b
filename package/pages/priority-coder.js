function PriorityCoder(m, n) {

  BasicCoder.call(this, m, n);

  this.y = 0;

  this.calculateOutput = function(i) { }

  this.activateInput(0);

  this.draw = function (canvas, context) {
    var width = 80;
    var height = 400;

    this.drawBasic(width, height, canvas, context);
    this.drawAdditional(width, height, canvas, context);
  }

  this.activateInput = function (i) {
    this.input[i] = this.input[i] == 0 ? 1 : 0;
    var mostPriorityInput = this.input.lastIndexOf(1);

    if(mostPriorityInput == -1) {
      this.y = 0;
      this.output.fill('0');
    } else {
      this.y = 1;
      this.calculateOutput(mostPriorityInput);
    }
  };

  this.drawAdditional = function (width, height, canvas, context) {
    var downRight = new Point(canvas.width/2 + width/2, canvas.height - (canvas.height - height) / 2);

    // y
    var length = 10;

    var spacing = height / (this.input.length + 1);
    context.beginPath();
    var start = new Point(downRight.x, downRight.y - ((this.input.length-this.output.length)/2)*spacing);
    context.moveTo(start.x, start.y);
    context.lineTo(start.x + length, start.y);

    context.font = "14px Arial";
    var text = "y";
    var textWidth = context.measureText(text).width;
    context.fillText(text, start.x - 15 - textWidth, start.y + 5);

    context.fillText(this.y, start.x + length + 5, start.y + 5);
    context.stroke();

  }
}

PriorityCoder.prototype = Object.create(BasicCoder.prototype);
