function setupPriorityCoder(canvasName, coder) {

  // set canvas
  var canvas = document.getElementById(canvasName);
  var context = canvas.getContext("2d");

  canvas.addEventListener('mousedown', function(evt) {
    var rect = canvas.getBoundingClientRect();
    var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);

    for(var i = 0; i < coder.inputPositions.length; i++) {
        var p = coder.inputPositions[i];
        if(pos.x >= p.x && pos.x < p.x + p.height && pos.y >= p.y && pos.y < p.y + p.height) {
            coder.activateInput(i);
            coder.draw(canvas, context);
            break;
        }
    }
  }, false);

  return {coder: coder, canvas: canvas, context: context};
}
