function PriorityBCDCoder() {

  var n = 4;
  PriorityCoder.call(this, 10, 4);

  this.calculateOutput = function(i) {
    for (var j = 0; j < n; ++j) {
      this.output[n-j-1] = (i >> j) & 1;
    }
  }

  this.activateInput(0);
}

PriorityBCDCoder.prototype = Object.create(PriorityCoder.prototype);
