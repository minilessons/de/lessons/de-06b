function Multiplexer(canvas, context, input, selection, hasEnabling){
    this.canvas = canvas;
    this.context = context;
    this.input = input;
    this.selection = selection;
    this.hasEnabling = hasEnabling;
    this.inputPoints = [];
    this.selectionPoints = [];
    this.outputPoints = [];
    this.isEnabled = 1;
    this.initialized = false;
    this.pathVisible = false;

    this.getInput = function(){
        return this.input;
    }

    this.getInputPoints = function(){
        return this.inputPoints;
    }

    this.getOutputPoints = function(){
        return this.outputPoints;
    }

    this.getSelectionPoints = function(){
        return this.selectionPoints;
    }

    this.setEnabled = function(enabled){
        this.isEnabled = enabled;
    }

    this.setPathVisible = function(isVisible){
        this.pathVisible = isVisible;
    }

    this.width = 60;
    this.height = 120;
    
    this.getWidth = function(){
        return this.width;
    }

    this.getHeight = function(){
        return this.height;
    }

    this.setWidth = function(width){
        this.width = width;
    }

    this.setHeight = function(height){
        this.height = height;
    }

    this.fontSize = 14;
    this.setFontSize = function(fontSize){
        this.fontSize = fontSize;
    }

    this.topLeft = new Point(canvas.width/2 - this.width/2, (canvas.height - this.height) / 2);
    this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
    this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height / 6);
    this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + 5 * this.height / 6);

    this.setTopLeft = function(topLeft){
        this.topLeft = topLeft;
        this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
        this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height / 6);
        this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + 5 * this.height / 6);
    }
    
    var inputLength = 10;
    var inputSize = 13;

    this.drawAll = function(){
        context.clearRect(0, 0, canvas.width, canvas.height); 
        
        this.draw();
        this.drawEnabling();
        this.drawOutput();
        this.drawSelection();
        this.drawInput();
    }

    this.drawInput = function(){
        var inputCount = input.elems.length;
        var spacing = this.height / (inputCount + 1);
        input.positions = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.topLeft.x, this.topLeft.y + (i + 1)*spacing);
            context.beginPath();
            context.font = this.fontSize + "px Arial";
            context.fillText(input.elems[i], start.x - inputLength - inputSize, start.y + inputSize/2 - 3);        
            context.stroke();

            context.beginPath();
            context.setLineDash([2, 2]);
            context.rect(start.x - inputLength - inputSize - 3, start.y - inputSize/2, inputSize, inputSize);
            input.positions[i] = {x: start.x - inputLength - inputSize - 3, y: start.y - inputSize/2, height: inputSize};
            context.stroke();
            context.setLineDash([]);
        }

        if (this.initialized) return;
        this.initialized = true;
        var multiplexer = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            for(var i = 0; i < input.positions.length; i++) {
                var p = input.positions[i];
                if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                    input.elems[i] = 1 - input.elems[i];
                    multiplexer.drawAll();
                    break;
                }
            }
        }, false);
    }

    this.drawSelection = function(){
        var selectionCount = this.selection.elems.length;
        var spacing = this.width / (selectionCount + 1);
        this.selection.positions = [];

        var direction = (this.downRight.y - this.downLeft.y) / (this.downRight.x - this.downLeft.x);
        for (var i = 0; i < selectionCount; ++i){
            var x = this.downRight.x - (i + 1) * spacing;
            var y = this.downLeft.y + direction * (x - this.downLeft.x);

            var start = new Point(x, y);
            context.beginPath();
            context.font = this.fontSize + "px Arial";
            context.fillText(this.selection.elems[i], start.x - 3, start.y + inputLength + inputSize/2 + 5);        
            context.stroke();

            context.beginPath();
            context.setLineDash([2, 2]);
            context.rect(start.x - inputSize / 2, start.y + inputLength + 1, inputSize, inputSize);
            this.selection.positions[i] = {
                x: start.x - inputSize / 2, 
                y: start.y + inputLength + 1, 
                height: inputSize
            };
            context.stroke();
            context.setLineDash([]);
        }

        if (this.initialized) return;
        var mux = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            for(var i = 0; i < mux.selection.positions.length; i++) {
                var p = mux.selection.positions[i];
                if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                    mux.selection.elems[i] = 1 - mux.selection.elems[i];
                    mux.drawAll();
                    break;
                }
            }
        }, false);
    }

    this.drawOutput = function(){
        var output = getOutput(this.input.elems, this.selection.elems);
        if (this.isEnabled == 0){
            output = 0;
        }

        context.beginPath();
        var start = new Point(this.downRight.x, (this.downRight.y + this.topRight.y) / 2);
        context.fillText(output, start.x + inputLength + 5, start.y + 5);

        context.stroke();

        if (!this.pathVisible || !this.isEnabled) return;
        context.beginPath();
        context.setLineDash([2, 1]);
        context.strokeStyle = '#ef5350';
        context.lineWidth = 2;
        var activeInput = getOutputId(this.selection.elems);
        var inputCount = this.input.elems.length;
        var spacing = this.height / (inputCount + 1);
        var inputStart = new Point(this.topLeft.x, this.topLeft.y + (activeInput + 1)*spacing);

        context.moveTo(inputStart.x, inputStart.y);
        context.lineTo(inputStart.x - inputLength, inputStart.y);

        context.moveTo(inputStart.x, inputStart.y);
        context.lineTo(start.x, start.y);

        context.moveTo(start.x, start.y);
        context.lineTo(start.x + inputLength, start.y);

        context.stroke();
        context.setLineDash([]);
        context.strokeStyle = "black";
        context.lineWidth = 1;
    }

    
    this.enableBox = {x: 0, y:0, height:0};
    this.drawEnabling = function(){
        if (!hasEnabling) return;

        context.beginPath();
        var start = new Point(
            (this.topLeft.x + this.topRight.x) / 2,
            (this.topLeft.y + this.topRight.y) / 2
        );
        start.y -= 2 * inputLength;
        context.moveTo(start.x, start.y);

        context.font = this.fontSize + "px Arial";
        var text = this.isEnabled;
        var textWidth = context.measureText(text).width;
        context.fillText(text, start.x - textWidth / 2, start.y - 5);        
        context.stroke();

        context.beginPath();
        context.setLineDash([2, 2]);
        context.rect(start.x - inputSize/2, start.y - inputSize - 3, inputSize, inputSize);
        enableBox = {
            x: start.x - inputSize/2, 
            y: start.y - inputSize - 3, 
            height: inputSize
        };
        context.stroke();
        context.setLineDash([]);

        if (this.initialized) return;
        var mux = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            var p = enableBox;
            if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                mux.isEnabled = 1 - mux.isEnabled;
                mux.drawAll();
            }
        }, false);
    }
    
    this.draw = function() {
        //drawing the outline of the decoder
        context.beginPath();
        context.moveTo(this.topLeft.x, this.topLeft.y);
        context.lineTo(this.downLeft.x, this.downLeft.y);
        context.lineTo(this.downRight.x, this.downRight.y);
        context.lineTo(this.topRight.x, this.topRight.y);
        context.lineTo(this.topLeft.x, this.topLeft.y);
        context.stroke();

        //drawing the input lines
        var inputCount = this.input.elems.length;
        var spacing = this.height / (inputCount + 1);
        context.beginPath();
        this.inputPoints = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.topLeft.x, this.topLeft.y + (i + 1)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x - inputLength, start.y);
            this.inputPoints.push(new Point(start.x - inputLength, start.y));

            context.font = this.fontSize + "px Arial";
            context.fillText("d" + i, start.x + 3, start.y + 3);           
        }
        context.stroke();

        //drawing selection lines
        var selectionCount = this.selection.elems.length;
        //var selectionLineLength = lineLength(this.downLeft, this.downRight);
        spacing = this.width / (selectionCount + 1);
        context.beginPath();
        this.selectionPoints = [];
        var direction = (this.downRight.y - this.downLeft.y) / (this.downRight.x - this.downLeft.x);
        for (var i = 0; i < selectionCount; ++i){
            var x = this.downRight.x - (i + 1) * spacing;
            var y = this.downLeft.y + direction * (x - this.downLeft.x);

            var start = new Point(x, y);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x, start.y + inputLength);
            this.selectionPoints.push(new Point(start.x, start.y + inputLength));

            context.font = this.fontSize + "px Arial";
            var text = "s" + i;
            var textWidth = context.measureText(text).width;
            context.fillText(text, start.x - textWidth / 2, start.y - 4);
        }
        context.stroke();

        
        //drawing the output line
        context.beginPath();
        this.outputPoints = [];
        var start = new Point(this.downRight.x, (this.downRight.y + this.topRight.y) / 2);
        context.moveTo(start.x, start.y);
        context.lineTo(start.x + inputLength, start.y);
        this.outputPoints.push(start.x + inputLength, start.y);

        context.font = this.fontSize + "px Arial";
        var text = "y";
        var textWidth = context.measureText(text).width;
        context.fillText(text, start.x - 5 - textWidth, start.y + 5);
        context.stroke();   
        
        //drawing the enable line
        if (hasEnabling){
            context.beginPath();
            var start = new Point(
                (this.topLeft.x + this.topRight.x) / 2, 
                (this.topLeft.y + this.topRight.y) / 2
            );
            context.moveTo(start.x, start.y);
            context.lineTo(start.x, start.y - 2 * inputLength);

            context.font = this.fontSize + "px Arial";
            context.fillText("E", start.x - 5, start.y + 15);
            context.stroke();
        }
    }

}

function setupMultiplexer(canvasName, initialInput, hasEnabling) {
    var canvas = document.getElementById(canvasName);
    var context = canvas.getContext("2d");
    var input = {elems: initialInput, positions: []};
    var selection = {elems: [], positions: []};

    var outputCount = 1;
    while (outputCount < input.elems.length){
        outputCount *= 2;
        selection.elems.push(0);
    }

    var multiplexer = new Multiplexer(canvas, context, input, selection, hasEnabling);
    multiplexer.setFontSize(12);

    return {canvas: canvas, context: context, multiplexer: multiplexer};
}
