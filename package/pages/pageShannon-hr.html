
<p>Shannonov teorem dekompozicije (poznat još i kao Shannonov teorem ekspanzije te Booleov teorem ekspanzije) nam govori sljedeće. Neka je \(f(x_1,\ldots,x_i,\ldots,x_n)\) proizvoljna Booleova funkcija od \(n\) varijabli \(x_1\ldots,x_n\). Tada se ona može zapisati na sljedeći način:
$$
  \begin{align}
  f(x_1,\ldots,x_i,\ldots,x_n) &amp;= \bar x_i \cdot f(x_1,\ldots,x_{i-1},0,x_{i+1},\ldots,x_n) + x_i \cdot f(x_1,\ldots,x_{i-1},1,x_{i+1},\ldots,x_n) \\
      &amp;= \bar x_i \cdot f_{x_i=0} + x_i \cdot f_{x_i=1}
  \end{align}
$$
gdje je \(x_i\) bilo koja od <i>n</i> varijabli o kojima ovisi funkcija.
U prvom retku izraz \(\bar x_i\) množi ono što se dobije kada se u funkciju \(f\) uvrsti da je varijabla \(x_i\) jednaka 0 dok izraz \(x_i\) množi ono što se dobije kada se u funkciju \(f\) uvrsti da je varijabla \(x_i\) jednaka 1. Drugi redak za te "ostatke" uvodi nova simbolička imena: \(f_{x_i=0}\) i \(f_{x_i=1}\).
</p>

<div class="mlImportant"><p>Uočite: dok je funkcija \(f\) funkcija od <i>n</i> varijabli, funkcije \(f_{x_i=0}\) i \(f_{x_i=1}\) su funkcije od <i>n</i>-1 varijable: one ne ovise o varijabli \(x_i\) jer su dobivene iz funkcije \(f\) uvrštavanjem konkretnih vrijednosti 0 odnosno 1 na mjesto varijable \(x_i\). Kako funkcije \(f_{x_i=0}\) i \(f_{x_i=1}\) predstavljaju ono što se dobije kada se uvrštavanjem konkretnih vrijednosti iz funkcije eliminira jedna varijabla (konkretno varijable \(x_i\)), zovemo ih <em>funkcije ostatka</em> odnosno <em>rezidualne funkcije</em>.</p><p>Rezidualne funkcije su uvijek jednostavnije od polazne funkcije.</p></div>

<p>Zašto ovaj teorem vrijedi, inuitivno možemo pokazati na jednostavnom primjeru. Promotrimo Booleovu funkciju \(f(A,B,C) = \bar A C + B \bar C\). Recimo da radimo dekompoziciju po varijabli <i>A</i>. Računamo:
$$
\begin{align}
 f_{A=0} &amp;= f(0,B,C) \\ &amp;= \left. \bar A C + B \bar C \right|_{A=0} \\ &amp;= \bar 0 \cdot C + B \bar C \\ &amp;= 1 \cdot C + B \bar C \\ &amp;= C + B \bar C \\ &amp;= (C+B) (C+\bar C) \\ &amp;= B+C &amp;&amp; \text{ali možemo nastaviti i dalje:} \\
 &amp;= B \cdot 1 + 1 \cdot C \\
 &amp;= B \cdot (\bar C + C) + (\bar B + B) \cdot C \\
 &amp;= B \bar C + B C + \bar B C + B C \\
 &amp;= B \bar C + B C + \bar B C \\ \\

 f_{A=1} &amp;= f(1,B,C) \\ &amp;= \left. \bar A C + B \bar C \right|_{A=1} \\ &amp;= \bar 1 \cdot C + B \bar C \\ &amp;= 0 \cdot C + B \bar C \\ &amp;= B \bar C
\end{align}
$$
Prema Shannovom teoremu, funkciju \(f\) tada možemo dekomponirati na sljedeći način:
$$
\begin{align}
 f(A,B,C) &amp;= \bar A \cdot f_{A=0} + A \cdot f_{A=1} \\ 
 &amp;= \bar A \cdot (B+C) + A \cdot B \bar C \\
 &amp;= \bar A \cdot (B \bar C + B C + \bar B C) + A \cdot B \bar C \\
\end{align}
$$
</p>

<p>Zaboravimo sada na tren čitavu priču o dekompoziciji Booleove funkcije i prisjetimo se da se svaka Booleova funkcija može prikazati u obliku sume minterma. Pa napravimo to za našu funkciju:
$$
\begin{align}
 f(A,B,C) &amp;= \bar A C + B \bar C \\
 &amp;= \bar A \cdot 1 \cdot C + 1 \cdot B \cdot \bar C \\
 &amp;= \bar A \cdot (\bar B + B) \cdot C + (\bar A + A) \cdot B \cdot \bar C \\
 &amp;= \bar A \cdot \bar B \cdot C + \bar A \cdot B \cdot C + \bar A \cdot B \cdot \bar C + A \cdot B \cdot \bar C
\end{align}
$$
Sada možemo odabrati bilo koju od varijabli i napraviti grupiranje. Idemo to napraviti po varijabli A. Pronađimo sve minterme koji u sebi imaju \(\bar A\) i grupirajmo ih pa izlučimo taj \(\bar A\); potom pronađimo sve minterme koji u sebi sadrže \(A\) pa ih grupirajmo i izlučimo taj \(A\). Evo postupka:
$$
\begin{align}
 f(A,B,C) &amp;= \bar A \cdot \bar B \cdot C + \bar A \cdot B \cdot C + \bar A \cdot B \cdot \bar C + A \cdot B \cdot \bar C \\
 &amp;= (\bar A \cdot \bar B \cdot C + \bar A \cdot B \cdot C + \bar A \cdot B \cdot \bar C) + A \cdot B \cdot \bar C \\
 &amp;= \bar A \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + A \cdot (B \cdot \bar C) \\
\end{align}
$$
I sada je dovoljno uočiti tri stvari.
</p>
<ol class="mlSpread">
<li>Funkciju \(f\) uspjeli smo zapisati u teoremom definiranom obliku \(\bar A \cdot f_1 + A \cdot f_2\) gdje su u ovom konkretnom slučaju \(f_1 = \bar B \cdot C + B \cdot C + B \cdot \bar C\) i \(f_2 = B \cdot \bar C\).</li>
<li>Ako u funkciju \(f\) uvrstimo da je \(A=0\), ostat će nam upravo \(f_1\). Pokažimo to:
$$
\begin{align}
  f(0,B,C) &amp;= \left. \bar A \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + A \cdot (B \cdot \bar C) \right| _{A=0}\\
    &amp;= \bar 0 \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + 0 \cdot (B \cdot \bar C) \\
    &amp;= 1 \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + 0 \cdot (B \cdot \bar C) \\
    &amp;= \bar B \cdot C + B \cdot C + B \cdot \bar C \\
    &amp;= f_1
\end{align}
$$
a upravo smo tu funkciju označili kao rezidualnu funkciju \(f_{A=0}\) za koju smo rekli da je dobivamo tako da u početnu funkciju uvrstimo da je \(A=0\); sada vidite zašto.</li>
<li>Ako u funkciju \(f\) uvrstimo da je \(A=1\), ostat će nam upravo \(f_2\). Pokažimo to:
$$
\begin{align}
  f(1,B,C) &amp;= \left. \bar A \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + A \cdot (B \cdot \bar C) \right| _{A=1}\\
    &amp;= \bar 1 \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + 1 \cdot (B \cdot \bar C) \\
    &amp;= 0 \cdot (\bar B \cdot C + B \cdot C + B \cdot \bar C) + 1 \cdot (B \cdot \bar C) \\
    &amp;= B \cdot \bar C \\
    &amp;= f_2
\end{align}
$$
a upravo smo tu funkciju označili kao rezidualnu funkciju \(f_{A=1}\) za koju smo rekli da je dobivamo tako da u početnu funkciju uvrstimo da je \(A=1\); sada vidite zašto.</li>
</ol>

<p>Usporedite izraze koje smo dobili Shannonovm dekompozicijom i izraze koje smo dobili izravnom manipulacijom zapisa funkcije u obliku sume minterma i uvjerite se da su identični.</p>

<p>Ako se pitate što se događa ako funkcije nema minterma koji bi primjerice imao komplementiranu varijablu po kojoj radimo raspis, možemo i to probati. Uzmimo kao primjer funkciju \(f(A,B,C)=A \cdot B + A \cdot C\). Prevođenjem funkcije u zapis sume minterma dobivamo:
$$
\begin{align}
 f(A,B,C) &amp;= A \cdot B + A \cdot C \\
          &amp;= A \cdot B \cdot 1 + A \cdot 1 \cdot C \\
          &amp;= A \cdot B \cdot (\bar C + C) + A \cdot (\bar B + B) \cdot C \\
          &amp;= A \cdot B \cdot \bar C + A \cdot B \cdot C + A \cdot \bar B \cdot C + A \cdot B \cdot C \\
          &amp;= A \cdot B \cdot \bar C + A \cdot B \cdot C + A \cdot \bar B \cdot C \\
          &amp;= A \cdot (B \cdot \bar C + B \cdot C + \bar B \cdot C) \\
\end{align}
$$
i to evidentno nije u obliku koji Shannonov teorem tvrdi da će biti. Ili je?
</p>

<p>
I naravno, u pravu ste. To je u tom obliku. Naime, prema postulatima Booleove algebre, dodavanjem nule nećemo promijeniti vrijednost funkcije. A ako nulu potom pomnožimo s nekim drugim izrazom, recimo s \(\bar A\), to i dalje ostaje nula pa ništa ne mijenja. Idemo to napraviti.
$$
\begin{align}
 f(A,B,C) &amp;= A \cdot (B \cdot \bar C + B \cdot C + \bar B \cdot C) \\
          &amp;= 0 + A \cdot (B \cdot \bar C + B \cdot C + \bar B \cdot C) \\
          &amp;= \bar A \cdot 0 + A \cdot (B \cdot \bar C + B \cdot C + \bar B \cdot C) \\
          &amp;= \bar A \cdot (0) + A \cdot (B \cdot \bar C + B \cdot C + \bar B \cdot C)
\end{align}
$$
i to je to. Dobili smo upravo izraz oblika \(\bar A \cdot f_{A=0} + A \cdot f_{A=1}\) gdje je, prema našim izračunima, \(f_{A=0}=0\) a \(f_{A=1}=B \cdot \bar C + B \cdot C + \bar B \cdot C\).
</p>

<p>Nije potrebno, ali pokažimo još i da to isto daje izravna uporaba Shannonove dekompozicije. Uvrštavanjem nule odnosno jedinice na mjesto varijable \(A\) u funkcijski izraz dobivamo:
$$
\begin{align}
  f_{A=0} &amp;= f(0,B,C) \\
    &amp;= \left. A \cdot B + A \cdot C \right| _{A=0}\\
    &amp;= 0 \cdot B + 0 \cdot C \\
    &amp;= 0 \\ \\
  f_{A=1} &amp;= f(1,B,C) \\
    &amp;= \left. A \cdot B + A \cdot C \right| _{A=0}\\
    &amp;= 1 \cdot B + 1 \cdot C \\
    &amp;= B+C
\end{align}
$$
čime smo dobili upravo jednake funkcije kao i izravnim raspisivanjem sume minterma. No to nas ne smije iznenaditi: raspis sume minterma dali smo upravo da bismo pokazali da Shannonov teorem vrijedi.</p>

<div class="mlNote">
<p>Shannonova dekompozicija koristan je alat kada složeniju funkciju trebamo dekomponirati (odnosno prikazati) na izraz koji sadrži jednostavnije Booleove funkcije. Kod Shannonove dekompozicije, te se jednostavnije funkcije nazivaju rezidualne funkcije i one su funkcije od <i>n</i>-1 varijable.</p>
</div>

<p>Shannonova dekompozicija ima i svoju izravnu sklopovsku implementaciju u obliku jednog od standardnih kombinacijskih modula: multipleksora.</p>


