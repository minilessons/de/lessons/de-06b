function PriorityBinaryCoder(n) {

  PriorityCoder.call(this, 1<<n, n);

  this.calculateOutput = function(i) {
    for (var j = 0; j < n; ++j) {
      this.output[n-j-1] = (i >> j) & 1;
    }
  }

  this.activateInput(0);
}

PriorityBinaryCoder.prototype = Object.create(PriorityCoder.prototype);
