function Point(x, y){
    this.x = x;
    this.y = y;
}

function getOutput(input, selection){
    var sol = getOutputId(selection);

    return input[sol];
}

function getOutputId(selection){
    var sol = 0;
    var powerOfTwo = 1;
    for (var i = 0; i < selection.length; ++i){
        if (selection[i] == 1){
            sol += powerOfTwo;
        }

        powerOfTwo *= 2;
    }

    return sol;
}

function lineLength(start, end){
    var a = start.x * end.x;
    var b = start.y * end.y;

    return Math.sqrt(a + b);
}